#ifndef LIBFILTER_APP_H
#define LIBFILTER_APP_H

#include <string>
#include <map>

#include "conductor/setup.h"
#include "cxxopts.h"
#include "erased_stats.h"
#include "sysfilter.h"

namespace ExtractOpts {
// Option sections
const std::string OPTS_GENERAL = "General";
const std::string OPTS_OUTPUT = "Output";
const std::string OPTS_EXCLUDE = "Exclude";

// Option strings
const std::string OPT_MAIN_EXEC = "main_exec";
const std::string OPT_HELP = "help";
const std::string OPT_HELP_SHORT = "h";
const std::string OPT_DL_FILE = "dl-file";
const std::string OPT_EXTRA_DEP = "extra-as-dep";
const std::string OPT_VERBOSE = "verbose";
const std::string OPT_VERBOSE_SHORT = "v";
const std::string OPT_OUTPUT = "output";
const std::string OPT_OUTPUT_SHORT = "o";
const std::string OPT_JSON = "json-stats";
const std::string OPT_JSON_SHORT = "j";
const std::string OPT_EXCLUDE = "exclude";
const std::string OPT_EXCLUDE_SHORT = "e";
}

class Libfilter {
private:
    std::string helpstr;

    Sysfilter sf;
    ConductorSetup *setup;

    struct Config {
        std::ostream *outputStream = nullptr;
        std::string outputFile;
        std::vector<char *> sysfilterArgs = {};
        std::map<std::string, std::set<std::string>> excludeFunctions = {};
        std::set<std::string> excludeLibraries = {};

        std::string mainExecPath;
        char *mainExecPath_c_str = NULL;
        std::string mainExecName;
        std::string outputJsonPath;
    } config;

public:
    Libfilter() = default;

    Libfilter(const Libfilter &) = delete;
    Libfilter &operator=(const Libfilter &) = delete;

    Libfilter(Libfilter &&) = default;
    Libfilter &operator=(Libfilter &&) = default;

    ~Libfilter() {
        if (config.mainExecPath_c_str) {
            free(config.mainExecPath_c_str);
        }

        if ((config.outputFile != "") && (config.outputStream != nullptr)) {
            delete config.outputStream;
        }
    };
    int parse(int argc, char **argv);
    int parse();
    void extract();
    void extractConfig(cxxopts::Options opts, int argc, char **argv);
    void buildOptions(cxxopts::Options &opts);
    void printUsage();
    std::string getMainExecPath();
    std::string getOutputFilePath();
    std::vector<std::string> getLibraryPaths();
    bool libInExcludeList(std::string library_path);
    bool functionInExcludeList(
        std::string library_name, std::string function_name);
};

#endif
