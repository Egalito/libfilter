#ifndef LIBFILTER_ERASER_H
#define LIBFILTER_ERASER_H

int erase_function(int fd, unsigned int byte_start, unsigned int byte_len);
int erase_from_file(const char *binary_path, const char *erase_path);
int erase_from_range(
    const char *binary_path, unsigned int byte_start, unsigned int byte_len);

#endif
