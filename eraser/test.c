#include <stdio.h>

int foo(int bar) {
    int baz = bar << 2;
    return baz * 5;
}

int main(int argc, char** argv) {
    int res = foo(argc + (int)sizeof(argv));
    printf("%d\n", res);
    return 0;
}
