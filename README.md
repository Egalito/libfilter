# `libfilter`

This is the repository for the `libfilter` project, which statically analyzes
binaries to automatically identify unused code in their dynamic shared
libraries and remove it.

The project is split into two components:
 - An [extraction](extraction/README.md) tool, based on
   [sysfilter](https://gitlab.com/egalito/sysfilter), for extracting the
   set of shared library functions that are not used by the program.
 - An [eraser](eraser/README.md) tool that removes unused functions from their
   respective binaries.

Documentation and source code for each component is located their respective
directories in the repository root. See the `README` in each directory for
details on how to build and run each tool.

Further information about the design and implementation of `libfilter` can be
found in our [paper](doc/libfilter_dtrap2020.pdf) at [DTRAP](https://dl.acm.org/toc/dtrap/2020/1/4).

```
@article{libfilter_dtrap2020,
	title		= {{Large-scale Debloating of Binary Shared Libraries}},
	author		= {Agadakos, Ioannis and Demarinis, Nicholas
				and Jin, Di and Williams-King, Kent
				and Alfajardo, Jearson
				and Shteinfeld, Benjamin
				and Williams-King, David
				and Kemerlis, Vasileios P.
				and Portokalidis, Georgios},
	journal		= {Digital Threats: Research and Practice},
	volume		= {1},
	number	 	= {4},
	pages	 	= {1--28},
	year		= {2020},
}

@inproceedings{nibbler_acsac2019,
	title		= {{Nibbler: Debloating Binary Shared Libraries}},
	author		= {Agadakos, Ioannis and Jin, Di
				and Williams-King, David
				and Kemerlis, Vasileios P.
				and Portokalidis, Georgios},
	booktitle	= {Annual Computer Security Applications Conference
				(ACSAC)},
	pages		= {70--83},
	year		= {2019}
}
```


## Requirements

In addition to libraries required by the `eraser` and `extraction` tools, `libfilter` requires [pre-commit](https://pre-commit.com).


## Cloning

This repository uses several git submodules for dependencies of the extraction
tool. To clone this repository, you __must__ use SSH and
have SSH access configured for [Gitlab](https://gitlab.com) __and__ [Github](https://github.com/).

Before cloning, please test your public keys are set up by connecting
to both services:
```
$ ssh git@gitlab.com
$ ssh git@github.com
```
If you receive a failure message for either service, check your
account settings and SSH keys to ensure you are connecting with a
public key attached to your account.

Once you are sure your public keys are configured, you can clone the
repository recursively with:

```
git clone --recursive git@gitlab.com:egalito/libfilter
```

# Compilation

To compile the tool on `x86-64`, run the following from the
root directory of the repository:
```
$ make
```

Parallel builds are encouraged due to the size of the codebase.
Also make sure you satisfy the dependencies listed at
[extraction/README](extraction/README.md) __and__ [eraser/README](eraser/README.md).


# Usage

In addition to running the extract and erase tools independently,
the `libfilter_main` tool can be used to handle the entire filtering process.

An example call to this tool from the root of the repository looks like:
`./app/libfilter_main path/to/executable`

## License

This software uses the [BSD License](./COPYING).
