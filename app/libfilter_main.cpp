#include "libfilter.h"
#include <experimental/filesystem>
#include <fstream>
#include <string>
#include <vector>

namespace fs = std::experimental::filesystem;

const std::string ERASE_SECTION_PREFIX = "module-";
const std::string ERASE_FILE_EXT = ".erase";
const std::string ERASE_DIR_NAME = "_libfilter_erase";
const std::string DEFAULT_LIB_DIR = "_libfilter";
constexpr uint BUFFER_SIZE = 2048;

namespace MainOpts {
// Option strings
const std::string OPT_LIB_DIR = "lib-dir";
const std::string OPT_LIB_DIR_SHORT = "l";
const std::string OPT_OUT_EXEC = "exec-out";
const std::string OPT_OUT_EXEC_SHORT = "x";
}

struct MainConfig {
    fs::path outputExec;
    fs::path relativeLibDir;
    fs::path absoluteLibDir;
} config;

extern "C" int erase_from_file(
    const char *executable_path, const char *erase_path);

/**
 * @brief Separate and format extracted erase data into individual files for
 * each executable/library.
 * @details The original erase file is separated by section, each section
 * corresponding to a library or executable. Resulting files only contain the
 * offsets and number of bytes to erase for each section.
 *
 * @param erase_path Path to erase data obtained from extraction process.
 * @param executable_name Filename of the main executable.
 * @return fs::path Directory containing individual erase files.
 */
fs::path split_erase_data(fs::path erase_path, std::string executable_name) {
    auto tmp_dir = fs::temp_directory_path() / ERASE_DIR_NAME;
    fs::remove_all(tmp_dir);
    fs::create_directories(tmp_dir);

    std::string exec_erase_name = ERASE_SECTION_PREFIX + executable_name +
                                  ERASE_FILE_EXT;
    fs::path current_efile = tmp_dir / exec_erase_name;

    std::ifstream erase_stream(erase_path);
    std::ofstream section_stream(current_efile);

    std::string line;
    getline(erase_stream, line);  // Ignore the erase section label
    while (getline(erase_stream, line)) {
        // Start a new erase section
        if (line.find(ERASE_SECTION_PREFIX) != std::string::npos) {
            section_stream.close();

            line.append(ERASE_FILE_EXT);
            current_efile = tmp_dir / line;
            std::cout << "Writing erase file " << current_efile.string()
                      << std::endl;
            section_stream = std::ofstream(current_efile);
        }
        // Append only the offset and number of bytes to erase section
        else {
            auto start = line.find(": ");
            section_stream << line.substr(start + 2) << "\n";
        }
    }
    section_stream.close();
    erase_stream.close();
    return tmp_dir;
}

/**
 * @brief Update the run-time search path of an executable
 *
 * @param executable_path Path to the executable to be modified
 * @param new_rpath New rpath directory
 */
void update_exec_rpath(std::string executable_path, std::string new_rpath) {
    std::string patchelf_cmd = "patchelf --set-rpath ";
    patchelf_cmd.append(new_rpath);
    patchelf_cmd.append(" ");
    patchelf_cmd.append(executable_path);

    std::array<char, BUFFER_SIZE> buffer;
    auto pipe = popen(patchelf_cmd.c_str(), "r");
    while (fgets(buffer.data(), buffer.size(), pipe) != nullptr) {
        std::string line = buffer.data();
        std::cout << line << std::endl;
    }
    if (pclose(pipe)) {
        throw std::runtime_error(
            "patchelf returned an error while updating the rpath");
    }
}

/**
 * @brief Helper function for finding erase files corresponding to an executable
 * or library.
 *
 * @param file_to_erase executable/library path
 * @param erase_files erase file paths
 * @return fs::path erase file for file_to_erase (empty if nothing
 * found)
 */
fs::path find_erase_file(fs::path file_to_erase, fs::path erase_file_dir) {
    std::string expected_filename = ERASE_SECTION_PREFIX +
                                    file_to_erase.filename().string() +
                                    ERASE_FILE_EXT;
    auto expected_path = erase_file_dir / expected_filename;

    if (fs::exists(expected_path)) {
        return expected_path;
    }
    return "";
}

/**
 * @brief Identify candidates for erasure.
 *
 * @param extractor Handles parsing input arguments and generating a list of
 * functions to erase.
 */
void libfilter_extract(Libfilter &extractor) {
    // Run the extraction
    if (extractor.parse()) {
        throw std::runtime_error("Extraction failed");
    }
}

/**
 * @brief Erase all all unused functions identified during the extract process.
 *
 * @param executable_path Path to the executable to erase.
 * @param erase_path Path to the extraction output.
 */
void libfilter_erase(Libfilter &extract_tool) {
    auto executable_path = fs::absolute(extract_tool.getMainExecPath());
    auto erase_path = fs::absolute(extract_tool.getOutputFilePath());
    auto libs = extract_tool.getLibraryPaths();

    // Copy the input executable to the output destination
    if (config.outputExec != executable_path) {
        fs::copy(executable_path, config.outputExec);
    }
    // Copy all the original libraries into the library dir
    std::vector<fs::path> libraries_to_erase{};
    for (int i = 1; i < libs.size(); i++) {
        fs::path lib = libs[i];
        libraries_to_erase.push_back(config.absoluteLibDir / lib.filename());
        fs::copy(lib, libraries_to_erase.back());
    }

    auto erase_dir = split_erase_data(erase_path, config.outputExec.filename());

    auto exec_erase_file = find_erase_file(config.outputExec, erase_dir);
    if (exec_erase_file != "") {
        erase_from_file(config.outputExec.c_str(), exec_erase_file.c_str());
        std::string new_rpath = "'$ORIGIN/" + config.relativeLibDir.string() +
                                "'";
        if (!libraries_to_erase.empty()) {
            update_exec_rpath(config.outputExec, new_rpath);
        }
    }

    for (auto erase_lib : libraries_to_erase) {
        auto erase_file = find_erase_file(erase_lib, erase_dir);
        if (erase_file != "") {
            erase_from_file(erase_lib.c_str(), erase_file.c_str());
            update_exec_rpath(erase_lib, "'$ORIGIN'");
        }
    }
}

void printUsage(cxxopts::Options opts) {
    std::cout << "Libfilter Main" << std::endl;
    std::cout << opts.help({ExtractOpts::OPTS_GENERAL, ExtractOpts::OPTS_OUTPUT,
                     ExtractOpts::OPTS_EXCLUDE})
              << std::endl;
}

cxxopts::Options buildOptions(Libfilter &extract_tool) {
    cxxopts::Options opts("libfilter",
        "Tool to remove unused functions from executables and their libraries");

    extract_tool.buildOptions(opts);

    auto out_opt = MainOpts::OPT_OUT_EXEC_SHORT + "," + MainOpts::OPT_OUT_EXEC;
    auto lib_opt = MainOpts::OPT_LIB_DIR_SHORT + "," + MainOpts::OPT_LIB_DIR;
    opts.add_options(ExtractOpts::OPTS_OUTPUT)(lib_opt,
        "Directory used for output libraries (relative to the output "
        "executable)",
        cxxopts::value<std::string>()->default_value(DEFAULT_LIB_DIR))(out_opt,
        "Path to output executable",
        cxxopts::value<std::string>()->default_value(""));

    return opts;
}

void extractConfig(
    cxxopts::Options opts, Libfilter &extract_tool, int argc, char **argv) {
    try {
        std::vector<char *> extract_args(argv, argv + argc);
        auto copt = opts.parse(argc, argv);

        if (copt[ExtractOpts::OPT_HELP].as<bool>()) {
            throw std::runtime_error("Help requested, so not doing any work.");
        }

        if (copt.count(ExtractOpts::OPT_MAIN_EXEC) != 1) {
            throw std::runtime_error("Main executable not provided!");
        }
        if (copt.count(MainOpts::OPT_OUT_EXEC) > 1) {
            throw std::runtime_error("Multiple output executable paths given!");
        }
        if (copt.count(MainOpts::OPT_OUT_EXEC) == 1) {
            config.outputExec = copt[MainOpts::OPT_OUT_EXEC].as<std::string>();
        }
        else {
            config.outputExec = copt[ExtractOpts::OPT_MAIN_EXEC]
                                    .as<std::string>();
        }
        config.outputExec = fs::absolute(config.outputExec);

        if (copt.count(MainOpts::OPT_LIB_DIR) > 1) {
            throw std::runtime_error("Multiple library directories given!");
        }
        config.relativeLibDir = copt[MainOpts::OPT_LIB_DIR].as<std::string>();
        config.absoluteLibDir = config.outputExec.parent_path() /
                                config.relativeLibDir;
        if (!fs::create_directories(config.absoluteLibDir)) {
            throw std::runtime_error(
                "Failed to create library directory! Make sure the library dir "
                "does not already exist.");
        }

        // If an extraction output file is not provided, generate one.
        auto tmp_out_path = fs::temp_directory_path() / "libfilter_extract.out";
        if (copt.count(ExtractOpts::OPT_OUTPUT) == 0) {
            extract_args.insert(extract_args.begin() + 1, (char *)"-o");
            extract_args.insert(
                extract_args.begin() + 2, (char *)tmp_out_path.c_str());
        }

        // Forward remaining parsing to the extract tool
        extract_tool.extractConfig(
            opts, extract_args.size(), extract_args.data());
    }
    catch (std::exception &e) {
        printUsage(opts);
        // Re-throw to main
        throw e;
    }
}

int main(int argc, char *argv[]) {
    try {
        Libfilter extractor;
        auto opts = buildOptions(extractor);
        extractConfig(opts, extractor, argc, argv);
        libfilter_extract(extractor);
        libfilter_erase(extractor);
        std::cout << "Libfilter finished successfully" << std::endl;
    }
    catch (std::exception &ex) {
        std::cout << ex.what() << std::endl;
        return 1;
    }

    return 0;
}
