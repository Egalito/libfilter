# The version.txt file is the official record of the version number. We use the
# contents of that file to set the project version for use in other CMake files.
file(READ "${CMAKE_CURRENT_LIST_DIR}/version.txt" ver)

string(REGEX MATCH "VERSION_MAJOR ([0-9]*)" _ ${ver})
set(CPACK_PACKAGE_VERSION_MAJOR ${CMAKE_MATCH_1})

string(REGEX MATCH "VERSION_MINOR ([0-9]*)" _ ${ver})
set(CPACK_PACKAGE_VERSION_MINOR ${CMAKE_MATCH_1})

string(REGEX MATCH "VERSION_PATCH ([0-9]*)" _ ${ver})
set(CPACK_PACKAGE_VERSION_PATCH ${CMAKE_MATCH_1})
set(CPACK_PACKAGE_VERSION
    ${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH})

set(CPACK_PACKAGE_NAME "libfilter")
set(CPACK_PACKAGE_DESCRIPTION_FILE "${CMAKE_CURRENT_LIST_DIR}/README.md")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "A tool for removing unused program and shared library functions")
set(CPACK_PACKAGE_VENDOR "Grammatech Inc.")
set(CPACK_PACKAGE_CONTACT libfilter@grammatech.com)
set(CPACK_PACKAGE_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_LIST_DIR}/COPYING")

set(CPACK_INSTALLED_DIRECTORIES CMAKE_CURRENT_LIST_DIR "cpack")
set(CPACK_PROJECT_CONFIG_FILE ${CMAKE_CURRENT_LIST_DIR}/cpack-config.cmake)
