#include "hello_goodbye_lib.h"
#include <stdio.h>

int main() {
    try {
        say_goodbye();
    }
    catch (...) {
        return 1;
    }
    return 0;
}
