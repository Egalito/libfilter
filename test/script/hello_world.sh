#!/bin/bash

# Make sure we fail if any sub-process fails
set -e

WORKING_DIR=$PWD
TEST_SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"
TEST_DIR="$TEST_SCRIPT_DIR/.."
EXAMPLE_DIR="$TEST_DIR/example"
TEST_HELLO_PATH="$EXAMPLE_DIR/hello_world"
TEST_GOODBYE_PATH="$EXAMPLE_DIR/goodbye_world"
TEST_FUNC_PATH="$EXAMPLE_DIR/hello_world_identified_funcs.txt"
ERASED_LIB_DIR="_libfilter/hello_world/libs"
ERASED_LIB_PATH="$EXAMPLE_DIR/$ERASED_LIB_DIR/hello_goodbye_lib.so"
VERIFY_SCRIPT_PATH="$TEST_SCRIPT_DIR/verify_erase.py"
EXCLUDE_LIST_PATH="$TEST_SCRIPT_DIR/exclude_list.txt"

# Check environment variables for a path to the libfilter tool
LIBFILTER_EXEC_DIR=${LIBFILTER_EXEC_DIR:-"$TEST_DIR/../app/"}

# Build the test executables and make sure they work
rm -rf $EXAMPLE_DIR/$ERASED_LIB_DIR
make -C $EXAMPLE_DIR clean
make -C $EXAMPLE_DIR

$TEST_HELLO_PATH
$TEST_GOODBYE_PATH

# Run libfilter on hello_world
$LIBFILTER_EXEC_DIR/libfilter_main -j $TEST_FUNC_PATH -e $EXCLUDE_LIST_PATH -l $ERASED_LIB_DIR $TEST_HELLO_PATH

# Verify erased functions
python3 $VERIFY_SCRIPT_PATH -b $TEST_HELLO_PATH -j $TEST_FUNC_PATH -e $EXCLUDE_LIST_PATH

# Make sure hello world still works
$TEST_HELLO_PATH

# Make sure goodbye world is broken if it uses the stripped library
cp $ERASED_LIB_PATH $EXAMPLE_DIR

trap 'case $? in
        139) echo "Test Passed!" && exit 0;;
      esac' EXIT
echo "Testing goodbye world - Expecting segfault"
$TEST_GOODBYE_PATH
echo "Library function still accessible, test failed"
exit 1
